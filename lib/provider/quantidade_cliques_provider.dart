import 'package:flutter/material.dart';

class QuantidadeCliquesProvider with ChangeNotifier {
  int _quantidade = 0;

  int get quantidade => _quantidade;

  set quantidade(int quantidade) {
    _quantidade = quantidade;

    notifyListeners();
  }
}
