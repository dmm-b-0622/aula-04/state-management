import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management/provider/quantidade_cliques_provider.dart';

class CampoQuantidadeDeDados extends StatefulWidget {
  const CampoQuantidadeDeDados({Key? key}) : super(key: key);

  @override
  State<CampoQuantidadeDeDados> createState() => _CampoQuantidadeDeDadosState();
}

class _CampoQuantidadeDeDadosState extends State<CampoQuantidadeDeDados> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text(
          'Você apertou o botao:',
        ),
        Consumer<QuantidadeCliquesProvider>(
          builder: (context, QuantidadeCliquesProvider value, child) => Text(
            '${value.quantidade}',
            style: Theme.of(context).textTheme.headline4,
          ),
        ),
      ],
    );
  }
}
